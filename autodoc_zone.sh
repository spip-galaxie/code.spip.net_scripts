# se mettre dans le bon répertoire de travail
RACINE=~/www/spip/codes/scripts.code.spip.net
SORTIES=~/www/spip/codes/code.plugins.spip.net
cd $RACINE

if [ $# = 0 ]
then
autodoc/bin/autodoc_helper from:file --avec_boussole_spip \
	--sorties=$SORTIES
else
autodoc/bin/autodoc_helper from:file --avec_boussole_spip \
	--sorties=$SORTIES \
	--prefixe=$1
fi