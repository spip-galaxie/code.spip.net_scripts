# Fonctionne presque, mais il manque des détails spécifiques
# pour le site tel que l'url racine, la boussole spip en js,
# (et anciennement les propositions de code)
#php autodoc/bin/autodoc_helper.php from:spip -s ../../www/autodoc

# pouvoir transmettre --force
OPTIONS=$@

# Pour corriger cela, on utilise un phpdoc.xml spécifique

# se mettre dans le bon répertoire de travail
RACINE=~/www/spip/codes/scripts.code.spip.net
cd $RACINE

# phpDocumentor préfère les nommages conventionnels ! 'phpdoc.xml' quelque soit le xml utilisé.
if [ -f phpdoc.xml ]; then
        rm phpdoc.xml
fi
cp phpdoc_spip.xml phpdoc.xml

# autodoc de SPIP
PREFIXE=spip
INPUT=$RACINE/work/input
SOURCE=$INPUT/$PREFIXE
TITRE=SPIP
DEST=$RACINE/../code.spip.net/autodoc
CACHE=$RACINE/work/cache/$PREFIXE
# pour correction si absence des css/js
TEMPLATE=$RACINE/autodoc/templates/zora

# remplacer des éléments de phpdoc.xml par nos données
# cela est fait car certains paramètres ne peuvent être transmis en ligne de commande
# particulièrement le chemin du cache parser/target
#sed -i "s/@PREFIXE@/$PREFIXE/g" phpdoc.xml
sed -i.bak "s|@CACHE@|$CACHE|g" phpdoc.xml && rm phpdoc.xml.bak
sed -i.bak "s|@DEST@|$DEST|g" phpdoc.xml && rm phpdoc.xml.bak


TITRE=--title="$TITRE"

# creer si inexistant
if [ ! -d "$DEST" ]; then
        mkdir $DEST
fi

# up de la source
if [ ! -d $INPUT ]; then
        mkdir -p $INPUT
fi
cd $INPUT
checkout.php spip -bmaster $PREFIXE
cd $RACINE


# lancer phpdoc
php $RACINE/autodoc/bin/autodoc \
        -c $RACINE/phpdoc.xml \
        -d $SOURCE \
        $TITRE \
        --parseprivate $OPTIONS


if [ ! -d $DEST/css ]; then
        echo "/!\ Répertoires CSS, JS non créés par PhpDocumentor"
        cp -R $TEMPLATE/css $DEST/css
        cp -R $TEMPLATE/js $DEST/js
        cp -R $TEMPLATE/bootstrap $DEST/bootstrap
        cp -R $TEMPLATE/images $DEST/images
        cp $TEMPLATE/favicon.png $DEST/favicon.png
fi