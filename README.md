Ces scripts exécutent la génération de documentation automatique.

### Arborescence

- /code.spip.net : le site SPIP
- /code.spip.net/autodoc : la doc auto de SPIP-dev
- /code.plugins.spip.net : la doc auto des plugins
- /scripts.code.spip.net : les scripts qui génèrent la doc
- /scripts.code.spip.net/autodoc : code source d’autodoc

### installation

- Copier le contenu ou cloner ce repo dans /scripts.code.spip.net
- cd /scripts.code.spip.net
- git clone git@git.spip.net:spip-galaxie/code.spip.net_autodoc.git autodoc
- cd autodoc
- composer install

### exécution

- ! necessite checkout.php
- cd /scripts.code.spip.net
- ./autodoc_spip.sh
- ./autodoc_zone.sh
